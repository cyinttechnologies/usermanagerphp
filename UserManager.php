<?php

namespace CYINT\ComponentsPHP\Managers;

use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Doctrine\UserManager as BaseUserManager;
use FOS\UserBundle\Util\CanonicalFieldsUpdater;
use CYINT\ComponentsPHP\Services\CryptographyService;
use FOS\UserBundle\Util\PasswordUpdaterInterface;

class UserManager extends BaseUserManager
{
    protected $Cryptography;

    public function __construct(CryptographyService $Cryptography, PasswordUpdaterInterface $passwordUpdater, CanonicalFieldsUpdater $usernameCanonicalizer, CanonicalFieldsUpdater $emailCanonicalizer, ObjectManager $om, $class) 
    {
        parent::__construct($passwordUpdater, $usernameCanonicalizer, $om, $class);

        $this->Cryptography = $Cryptography;
    }

    public function createUser()
    {
        $User = parent::createUser();
        $User->setCreated(time());
        $User->setCryptographyService($this->Cryptography);
        return $User;
    }

    public function updateUser(\FOS\UserBundle\Model\UserInterface $User)
    {
        return parent::updateUser($User);
    }

    public function findUserByEmail($email)
    {        
        $encrypted_email = $this->Cryptography->encrypt(strtolower(trim($email)));        
        $User = $this->findUserBy(array('email' => $encrypted_email));
        if(!empty($User))
            $User->setCryptographyService($this->Cryptography);
        return $User;

    }

    public function findUserBy(array $criteria)
    {
        $User = parent::findUserBy($criteria);
        if(!empty($User))
            $User->setCryptographyService($this->Cryptography);
        return $User;
    }

    public function findUserByToken($token)
    {
        $User = parent::findUserBy(array('confirmationToken'=>$token));
        if(!empty($User))
            $User->setCryptographyService($this->Cryptography);
        return $User;

    }
}
